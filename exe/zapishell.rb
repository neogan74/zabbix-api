#!/usr/bin/env ruby
require 'zabbix/api'
require 'optimist'
require 'amazing_print'
require 'pry'



opts = Optimist::options do
  opt :url, "URL up to but no including api_jsonrpc.php",type: :string,default:'http://localhost'
  opt :user, "User name to authenticate", type: :string, required: true
  opt :pass, "Pass to auth user with", type: :string, required: true
end

print "user.login: "
api = Zabbix::Api::Client.new(url: opts[:url])

ap api.login(user: opts[:user],pass:opts[:pass])

binding.pry

print "user.logout: "
ap api.logout
