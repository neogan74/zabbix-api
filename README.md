# Zabbix::Api

This zabbix api implementation for Ruby strives for three things:

- To the greatest extent possible, be syntactically & structurally identical to the API as it is described in the Zabbix documentation
- Be API version agnostic
- Be internally simple, unobtrusive, and low-maintenance

It accomplishes these goals primarily by deferring (via Faraday) to the Zabbix API itself for just about everything - it does not attempt to re-create or otherwise "fancy-up" the API.  The reasoning behind this is that the zabbix api itself is complex enough, and the Zabbix project can alter the specifics of the api at any time.  This library just provides Ruby semantics that closely follow the API specification.  The advantage to you is that you do not need to wrap your head around two APIs (the Zabbix API proper, and the API library's API)

**NOTE:**  This thing is brand new, and it works for me.  I'm certain I'm far from done adding stuff to it, but as it is at this moment it's good enough for me.  While I doubt it, there may be breaking changes going forward, so just "buyer beware".  I'll remove this note once I feel it's more or less stable.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'zabbix-api-simple'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install zabbix-api-simple

## Usage

Once you have an authenticated instance of the client, syntax is basically always this:

`clientinstance.object.verb(<param kv pairs>)`

If "object" is unknown, "Unknown zabbix object given" will be raised.

If this call results in an error from the API, a RuntimeError will be raised indicating the specifics.

Complete detail about the last transaction you attempted is available via `clientinstance.last`

e.g.:

```
#!/usr/bin/env ruby
require 'zabbix/api'
require 'optimist'
require 'amazing_print'



opts = Optimist::options do
  opt :url, "URL up to but no including api_jsonrpc.php",type: :string,default:'http://localhost'
  opt :user, "User name to authenticate", type: :string, required: true
  opt :pass, "Pass to auth user with", type: :string, required: true
  opt :hostname, "Host name to search for", type: :string, required: true
end

api = Zabbix::Api::Client.new(url: opts[:url])

api.login(user: opts[:user],pass:opts[:pass])

hosts = api.host.get(search: {host: opts[:hostname]})  # return value is an OpenStruct

hosts.each {|host|
    puts host.name
    puts host.hostid

    ap host.to_h
}


api.host.create(
    host: "mynewhost",
    interfaces: {
        type: 1,
        main: 1,
        useip: 1,
        ip: "1.2.3.4",
        dns: "",
        port: "10050"
    },
    groups: {groupid: "42"}
)

ap api.last # shows detail of the last transaction

ap api.logout
```

There's a little cli program that gets installed with the gem called zapishell.rb:

```
# zapishell.rb --help
Options:
  -u, --url=<s>     URL up to but no including api_jsonrpc.php (default: http://localhost)
  -s, --user=<s>    User name to authenticate
  -p, --pass=<s>    Pass to auth user with
  -h, --help        Show this message
```

It attempts to authenticate you to the given server, and then drops you to a pry prompt.  You can play around with the api in that by doing `api.object.verb(key: 'value pairs')`

Refer to the API documentation for your version of Zabbix for the particulars.

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/zabbix-api.
