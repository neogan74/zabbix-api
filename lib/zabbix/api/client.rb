module Zabbix
  module Api

    require 'faraday'
    require 'json'
    require 'set'
    require 'pry'

    module FaradayMiddleware

      class ZabbixApiRequest < Faraday::Middleware

        def initialize(app)
          super(app)
          @app=app
        end

        def on_request(env)
          env[:request_body][:jsonrpc] = "2.0"
          env[:request_body][:id] = "1"
          env[:request_body] = env[:request_body].to_json
        end

        def on_complete(env)
          env[:response_body] = JSON.parse(env[:response_body])
        end

      end

      Faraday::Request.register_middleware zabbix_api_request: -> { ZabbixApiRequest }

    end # module FaradayMiddleware




    class Client
      
      class << self
        attr_accessor :last
      end


      @@apiurl = 'api_jsonrpc.php'

      @@zabbix_objects = [:action,:alert,:apiinfo,:application,:configuration,
                          :correlation,:dashboard,:dhost,:dservice,:dcheck,
                          :drule,:event,:graph,:graphitem,:graphprototype,
                          :history,:host,:hostgroup,:hostinterface,
                          :hostprototype,:iconmap,:image,:item,:itemprototype,
                          :discoveryrule,:maintenance,:map,:mediatype,:problem,
                          :proxy,:screen,:screenitem,:script,:service,:task,
                          :template,:templatescreen,:templatescreenitem,
                          :trend,:trigger,:triggerprototype,:user,:usergroup,
                          :usermacro,:valuemap,:httptest].to_set


      attr_reader :conn,:token
      attr_accessor :zabobject


      def initialize(url: nil,timeout: 60)
        @conn = Faraday.new(
          url: url,
          headers: {'Content-Type' => 'application/json-rpc'},
          request: { timeout: timeout }
        ) do |conn|
          conn.request :zabbix_api_request
        end
        @zabobject = nil
      end

      def post(args)
        args[:params] = [] if not args.has_key?(:params) or args[:params].nil?
        last = @conn.post(@@apiurl, args)
        Client.last = last
        return last
      end

      def login(user: nil,pass: nil)
        res =post(method: 'user.login', params: {user: user, password:pass}, auth:nil)
        @token = res.body['result']
        OpenStruct.new(res.body)
      end

      def logout
        OpenStruct.new(post(method: 'user.logout', params: [], auth: @token).body)
      end

      def call(name, *args, &block)
          res = post(method: "#{name}", params: args.first, id: '1', auth: @token).body
          raise res['error'].awesome_inspect(plain: true)  if res.has_key?('error')
          if res.has_key?('result') and res['result'].class == Array
            res = res['result'].collect{|each| OpenStruct.new(each)}
          else
            res = OpenStruct.new(res)
          end
          return res
      end


      def method_missing(name, *args, &block)
        if @@zabbix_objects.include?(name)
          # Clone self cuz we want to be thread safe/recursable.  This will pop off the
          # stack after it's no longer referenced (and @zabobject will never change in the
          # original client instance)
          newcli = self.clone
          newcli.zabobject = name
          return newcli
        elsif @zabobject
          return call("#{@zabobject}.#{name}",args.first)
        else
          raise "Unknown zabbix object given: #{name}"
        end
      end

      def last
        Client.last
      end

    end

  end # module Api
end # module Zabbix
