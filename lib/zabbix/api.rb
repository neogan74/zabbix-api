# frozen_string_literal: true

require_relative "api/version"
require_relative "api/client"

module Zabbix
  module Api
    class Error < StandardError; end
    # Your code goes here...
  end
end
